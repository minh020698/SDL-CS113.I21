#include "FillColor.h"
#include <queue>
#include <stack>
#include <iostream>
#include <algorithm>
using namespace std;

//Get color of a pixel
SDL_Color getPixelColor(Uint32 pixel_format, Uint32 pixel)
{
	SDL_PixelFormat* fmt = SDL_AllocFormat(pixel_format);

	Uint32 temp;
	Uint8 red, green, blue, alpha;

	/* Get Red component */
	temp = pixel & fmt->Rmask;  /* Isolate red component */
	temp = temp >> fmt->Rshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Rloss;  /* Expand to a full 8-bit number */
	red = (Uint8)temp;

	/* Get Green component */
	temp = pixel & fmt->Gmask;  /* Isolate green component */
	temp = temp >> fmt->Gshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Gloss;  /* Expand to a full 8-bit number */
	green = (Uint8)temp;

	/* Get Blue component */
	temp = pixel & fmt->Bmask;  /* Isolate blue component */
	temp = temp >> fmt->Bshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Bloss;  /* Expand to a full 8-bit number */
	blue = (Uint8)temp;

	/* Get Alpha component */
	temp = pixel & fmt->Amask;  /* Isolate alpha component */
	temp = temp >> fmt->Ashift; /* Shift it down to 8-bit */
	temp = temp << fmt->Aloss;  /* Expand to a full 8-bit number */
	alpha = (Uint8)temp;

	SDL_Color color = { red, green, blue, alpha };
	return color;

}

//Get all pixels on the window
SDL_Surface* getPixels(SDL_Window* SDLWindow, SDL_Renderer* SDLRenderer) {
	SDL_Surface* saveSurface = NULL;
	SDL_Surface* infoSurface = NULL;
	infoSurface = SDL_GetWindowSurface(SDLWindow);
	if (infoSurface == NULL) {
		std::cerr << "Failed to create info surface from window in saveScreenshotBMP(string), SDL_GetError() - " << SDL_GetError() << "\n";
	}
	else {
		unsigned char * pixels = new (std::nothrow) unsigned char[infoSurface->w * infoSurface->h * infoSurface->format->BytesPerPixel];
		if (pixels == 0) {
			std::cerr << "Unable to allocate memory for screenshot pixel data buffer!\n";
			return NULL;
		}
		else {
			if (SDL_RenderReadPixels(SDLRenderer, &infoSurface->clip_rect, infoSurface->format->format, pixels, infoSurface->w * infoSurface->format->BytesPerPixel) != 0) {
				std::cerr << "Failed to read pixel data from SDL_Renderer object. SDL_GetError() - " << SDL_GetError() << "\n";
				delete[] pixels;
				return NULL;
			}
			else {
				saveSurface = SDL_CreateRGBSurfaceFrom(pixels, infoSurface->w, infoSurface->h, infoSurface->format->BitsPerPixel, infoSurface->w * infoSurface->format->BytesPerPixel, infoSurface->format->Rmask, infoSurface->format->Gmask, infoSurface->format->Bmask, infoSurface->format->Amask);
			}
			delete[] pixels;
		}
	}
	return infoSurface;
}

//Compare two colors
bool compareTwoColors(SDL_Color color1, SDL_Color color2)
{
	if (color1.r == color2.r && color1.g == color2.g && color1.b == color2.b && color1.a == color2.a)
		return true;
	return false;
}

void BoundaryFill4(SDL_Window *win, Vector2D startPoint, Uint32 pixel_format,
	SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{
	int x, y;
	stack<pair<int, int>> s;
	s.push(make_pair(startPoint.x, startPoint.y));
	pair<int, int> temp;
	
	SDL_Surface *surface = getPixels(win, ren);
	Uint32 *pixels = (Uint32 *)surface->pixels;
	Uint32 curPixel;
	Uint32 targetPixel = SDL_MapRGBA(surface->format, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	SDL_Color curColor;
	

	while (!s.empty())
	{
		temp = s.top();
		x = temp.first, y = temp.second;
		curPixel = pixels[(y * surface->w) + x];
		curColor = getPixelColor(pixel_format, curPixel);
		curColor.a = 255;

		if (compareTwoColors(curColor, boundaryColor) == false && compareTwoColors(curColor, fillColor) == false)
			pixels[(y * surface->w) + x] = targetPixel;

		x = temp.first - 1;
		curPixel = pixels[(y * surface->w) + x];
		curColor = getPixelColor(pixel_format, curPixel);
		curColor.a = 255;
		
		if (compareTwoColors(curColor, boundaryColor) == false && compareTwoColors(curColor, fillColor) == false)
		{
			s.push({ x,temp.second });
			continue;
		}

		x = temp.first + 1;
		curPixel = pixels[(y * surface->w) + x];
		curColor = getPixelColor(pixel_format, curPixel);
		curColor.a = 255;
		if (compareTwoColors(curColor, boundaryColor) == false && compareTwoColors(curColor, fillColor) == false)
		{
			s.push({ x,temp.second });
			continue;
		}

		y = temp.second - 1;
		curPixel = pixels[(y * surface->w) + x];
		curColor = getPixelColor(pixel_format, curPixel);
		curColor.a = 255;
		if (compareTwoColors(curColor, boundaryColor) == false && compareTwoColors(curColor, fillColor) == false)
		{
			s.push({ temp.first,y });
			continue;
		}

		y = temp.second + 1;
		curPixel = pixels[(y * surface->w) + x];
		curColor = getPixelColor(pixel_format, curPixel);
		curColor.a = 255;
		if (compareTwoColors(curColor, boundaryColor) == false && compareTwoColors(curColor, fillColor) == false)
		{
			s.push({ temp.first,y });
			continue;
		}

		s.pop();
	}
}


//======================================================================================================================
//=============================================FILLING TRIANGLE=========================================================

int maxIn3(int a, int b, int c)
{
	int lc = a;
	if (b > lc)
		c = b;
	if (c > lc)
		lc = c;
	return lc;
}

int minIn3(int a, int b, int c)
{
	int lc = a;
	if (b < lc)
		c = b;
	if (c < lc)
		lc = c;
	return lc;
}

void swap(Vector2D &a, Vector2D &b)
{
	Vector2D temp(a);
	a.set(b);
	b.set(temp);
}

void ascendingSort(Vector2D &v1, Vector2D &v2, Vector2D &v3)
{
	if (v1.y > v2.y) {
		swap(v1, v2);
	}

	if (v1.y > v3.y)
	{
		swap(v1, v3);
	}

	if (v2.y > v3.y)
	{
		swap(v2, v3);
	}
}

void TriangleFill1(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	int xleft = minIn3(v1.x, v2.x, v3.x);
	int xright = maxIn3(v1.x, v2.x, v3.x);
	Bresenham_Line(xleft, v1.y, xright, v1.y, ren);
}

void TriangleFill2(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float xleft = minIn3(v1.x, v2.x, v2.x);
	float xright = maxIn3(v1.x, v2.x, v2.x);
	float mleft = (v3.y - v1.y) / (v3.x - xleft);
	float mright = (v3.y - v1.y) / (v3.x - xright);
	int y = v1.y;
	while (y <= v3.y)
	{
		Bresenham_Line(int(xleft + 0.5), y, int(xright + 0.5), y, ren);
		xleft += 1 / mleft;
		xright += 1 / mright;
		y++;
	}
}

void TriangleFill3(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float xleft = minIn3(v2.x, v2.x, v3.x);
	float xright = maxIn3(v2.x, v2.x, v3.x);
	float mleft = (v3.y - v1.y) / (xleft - v1.x);
	float mright = (v3.y - v1.y) / (xright - v1.x);
	int y = v3.y;
	while (y >= v1.y)
	{
		Bresenham_Line(int(xleft + 0.5), y, int(xright + 0.5), y, ren);
		xleft -= (1 / mleft);
		xright -= (1 / mright);
		y--;
	}
}

void TriangleFill4(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float m = (v3.y - v1.y) / (v3.x - v1.x);
	float dx = (v2.y - v1.y) / m;
	float xIntersect = v1.x + dx;
	Vector2D intersect(xIntersect, v2.y);
	TriangleFill(v2, intersect, v3, ren, fillColor);
	TriangleFill(v1, v2, intersect, ren, fillColor);
}

void TriangleFill(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	ascendingSort(v1, v2, v3);
	if (v1.y == v2.y && v2.y == v3.y)
	{
		TriangleFill1(v1, v2, v3, ren, fillColor);
		return;
	}
	if (v1.y == v2.y && v2.y < v3.y)
	{
		TriangleFill2(v1, v2, v3, ren, fillColor);
		return;
	}
	if (v2.y == v3.y && v1.y < v2.y)
	{
		TriangleFill3(v1, v2, v3, ren, fillColor);
		return;
	}
	TriangleFill4(v1, v2, v3, ren, fillColor);
}
//======================================================================================================================
//===================================CIRCLE - RECTANGLE - ELLIPSE=======================================================
bool isInsideCircle(int xc, int yc, int R, int x, int y)
{
	if ((x - xc)*(x - xc) + (y - yc)*(y - yc) <= R*R)
		return true;
	return false;
}

void FillIntersection(int x1, int y1, int x2, int y2, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int x = min(x1,x2);
	while (x <= max(x1, x2))
	{
		if (isInsideCircle(xc, yc, R, x, y1))
			SDL_RenderDrawPoint(ren, x, y1);
		x++;
	}
}

bool isInsideEllispe(int xcE, int ycE, int a, int b, int x, int y)
{
	int dx = x - xcE;
	int	dy = y - ycE;
	return (dx * dx) / (a * a) + (dy * dy) / (b * b) <= 1;
}

void FillIntersectionEllipse(int x1, int y1, int x2, int y2, int xcE, int ycE, int a, int b,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int x = min(x1, x2);
	while (x <= max(x1, x2))
	{
		if (isInsideEllispe(xcE, ycE, a, b, x, y1))
			SDL_RenderDrawPoint(ren, x, y1);
		x++;
	}
}

void FillIntersectionRectangleCircle(Vector2D vTopLeft, Vector2D vBottomRight, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int x = min(vTopLeft.x, vBottomRight.x);
	int y = min(vTopLeft.y, vBottomRight.y);
	while (y != max(vTopLeft.y, vBottomRight.y))
	{
		while (x != max(vTopLeft.x, vBottomRight.x))
		{
			if (isInsideCircle(xc, yc, R, x, y))
				SDL_RenderDrawPoint(ren, x, y);
			x++;
		}
		y++;
		x = min(vTopLeft.x, vBottomRight.x);
	}
}

void RectangleFill(Vector2D vTopLeft, Vector2D vBottomRight, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int x = min(vTopLeft.x, vBottomRight.x);
	int y = min(vTopLeft.y, vBottomRight.y);
	while (y != max(vTopLeft.y, vBottomRight.y))
	{
		while (x != max(vTopLeft.x, vBottomRight.x))
		{
			SDL_RenderDrawPoint(ren, x, y);
			x++;
		}
		y++;
		x = min(vTopLeft.x, vBottomRight.x);
	}
}

void put4line(int xc, int yc, int x, int y, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	SDL_RenderDrawLine(ren, xc + y, yc + x, xc - y, yc + x);
	SDL_RenderDrawLine(ren, xc + x, yc + y, xc - x, yc + y);
	SDL_RenderDrawLine(ren, xc + x, yc - y, xc - x, yc - y);
	SDL_RenderDrawLine(ren, xc + y, yc - x, xc - y, yc - x);
}

void CircleFill(int xc, int yc, int R, SDL_Renderer *ren, SDL_Color fillColor)
{
	int x = R;
	int y = 0;
	int p = 1 - R;
	put4line(xc, yc, x, y, ren, fillColor);
	while (x > y)
	{
		if (p <= 0)
		{
			p += 2 * y + 3;
		}
		else
		{
			p += 2 * y - 2 * x + 5;
			x--;
		}
		y++;
		put4line(xc, yc, x, y, ren,fillColor);
	}
}

void FillIntersectionEllipseCircle(int xcE, int ycE, int a, int b, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int x = R;
	int y = 0;
	int p = 1 - R;
	FillIntersectionEllipse(xc + y, yc + x, xc - y, yc + x, xcE, ycE, a, b, ren, fillColor);
	FillIntersectionEllipse(xc + x, yc + y, xc - x, yc + y, xcE, ycE, a, b, ren, fillColor);
	FillIntersectionEllipse(xc + x, yc - y, xc - x, yc - y, xcE, ycE, a, b, ren, fillColor);
	FillIntersectionEllipse(xc + y, yc - x, xc - y, yc - x, xcE, ycE, a, b, ren, fillColor);
	while (x > y)
	{
		if (p <= 0)
		{
			p += 2 * y + 3;
		}
		else
		{
			p += 2 * y - 2 * x + 5;
			x--;
		}
		y++;
		FillIntersectionEllipse(xc + y, yc + x, xc - y, yc + x, xcE, ycE, a, b, ren, fillColor);
		FillIntersectionEllipse(xc + x, yc + y, xc - x, yc + y, xcE, ycE, a, b, ren, fillColor);
		FillIntersectionEllipse(xc + x, yc - y, xc - x, yc - y, xcE, ycE, a, b, ren, fillColor);
		FillIntersectionEllipse(xc + y, yc - x, xc - y, yc - x, xcE, ycE, a, b, ren, fillColor);
	}
}

void FillIntersectionTwoCircles(int xc1, int yc1, int R1, int xc2, int yc2, int R2,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int x = R1;
	int y = 0;
	int p = 1 - R1;
	FillIntersection(xc1 + y, yc1 + x, xc1 - y, yc1 + x, xc2, yc2, R2, ren, fillColor);
	FillIntersection(xc1 + x, yc1 + y, xc1 - x, yc1 + y, xc2, yc2, R2, ren, fillColor);
	FillIntersection(xc1 + x, yc1 - y, xc1 - x, yc1 - y, xc2, yc2, R2, ren, fillColor);
	FillIntersection(xc1 + y, yc1 - x, xc1 - y, yc1 - x, xc2, yc2, R2, ren, fillColor);
	while (x > y)
	{
		if (p <= 0)
		{
			p += 2 * y + 3;
		}
		else
		{
			p += 2 * y - 2 * x + 5;
			x--;
		}
		y++;
		FillIntersection(xc1 + y, yc1 + x, xc1 - y, yc1 + x, xc2, yc2, R2, ren, fillColor);
		FillIntersection(xc1 + x, yc1 + y, xc1 - x, yc1 + y, xc2, yc2, R2, ren, fillColor);
		FillIntersection(xc1 + x, yc1 - y, xc1 - x, yc1 - y, xc2, yc2, R2, ren, fillColor);
		FillIntersection(xc1 + y, yc1 - x, xc1 - y, yc1 - x, xc2, yc2, R2, ren, fillColor);
	}
}
